# OpenML dataset: pollution

https://www.openml.org/d/542

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

This is the pollution data so loved by writers of papers on ridge regression.
Source: McDonald, G.C. and Schwing, R.C. (1973) 'Instabilities of regression
estimates relating air pollution to mortality', Technometrics, vol.15, 463-
Variables in order:
PREC   Average annual precipitation in inches
JANT   Average January temperature in degrees F
JULT   Same for July
OVR65  % of 1960 SMSA population aged 65 or older
POPN   Average household size
EDUC   Median school years completed by those over 22
HOUS   % of housing units which are sound & with all facilities
DENS   Population per sq. mile in urbanized areas, 1960
NONW   % non-white population in urbanized areas, 1960
WWDRK  % employed in white collar occupations
POOR   % of families with income < $3000
HC     Relative hydrocarbon pollution potential
NOX    Same for nitric oxides
SO@    Same for sulphur dioxide
HUMID  Annual average % relative humidity at 1pm
MORT   Total age-adjusted mortality rate per 100,000


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/542) of an [OpenML dataset](https://www.openml.org/d/542). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/542/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/542/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/542/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

